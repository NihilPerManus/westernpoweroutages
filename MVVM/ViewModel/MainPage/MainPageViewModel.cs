﻿
using PowerOutageTester.Logging;
using PowerOutageTester.Outage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Data;
using System.Windows.Input;

namespace PowerOutageTester.MVVM.ViewModel
{
    public class MainPageViewModel : PippyPlatform.Observable.ObservableClass, IMainPageViewModel
    {

        //Using observable collections so that they are able to inform the User Interfaces of any kind of changes that occur, and refresh it accordingly.
        private ObservableCollection<SuburbOutageAggregate> _Suburbs = new ObservableCollection<SuburbOutageAggregate>(); //cbf making factory for these items
        private ObservableCollection<LogEntry> _GlobalLog = new ObservableCollection<LogEntry>();

        //These public properties are used for Data-binding to the UI via WPF. using "{Binding Suburbs}" in a collection control will allow you to use all the collected data
        public ObservableCollection<SuburbOutageAggregate> Suburbs { get { return _Suburbs; } set {  _Suburbs = value; RaisePropertyChanged(); } }
        public ObservableCollection<LogEntry> GlobalLog { get { return _GlobalLog; } set { _GlobalLog = value; RaisePropertyChanged(); } }

        //Should I ever get around to implementing a search field then here it is.
        public string SearchText { get { return _SearchText; } set { _SearchText = value; RaisePropertyChanged(); } }
        public bool ShowOperational { get { return _ShowOperational; } set { _ShowOperational = value; RaisePropertyChanged(); } }

        //I'm using this to beging the pull of information rather than the Constructor as it allows the GUI to show before it does any processing. 
        //During this time I should implement a loading bar to show the user there is work occuring in the background. for now it just shows blank...
        public ICommand Initialize => new PippyPlatform.Command.DelegateCommand(e =>  Begin());

        //create an update timer for use in this class
        private Timer updateTimer = new Timer();

        void Begin()
        {
            //Initially we will update the suburb list... this will pull down t he initial data
            UpdateSuburbs();

            //start the timer
            updateTimer.Start();
      

            Console.WriteLine($"{Suburbs.Count()} processed");

            //Make the timer raise an event every minute
            updateTimer.Interval = 1000 * 60; //One munite updates

            //Here is the lambda that runs when the event timer elapses: When one minute passed, UpdateSuburbs() is called
            updateTimer.Elapsed += (a,b) =>  UpdateSuburbs();
           
        }

        private void UpdateSuburbs()
        {

            Console.WriteLine("BEGIN UPDATE");
            _poweroutageService //From the poweroutage service
                ?.GetOutages() //get all the outages
                ?.ToList()  //create a list (This loads the entire Outage function so it's no longer lazy)
                ?.ForEach(o => //o will indicate the current outage *pulled from the service*
                {
                    //get the current outage we are dealing with that may already exist in the Suburbs
                    var current = Suburbs.FirstOrDefault(t => t.Suburb.Name == o.Suburb.Name);

                    //Is it one that already exists?
                    if (current == null) //If it's null then it's a new suburb. We will add it to our stored list of outages
                    {
                        Console.WriteLine($" adding {o.Suburb.Name}");
                        Console.WriteLine("NEW OUTAGE");
                       
                        Suburbs.Add(o);
                    }
                    else //update the one we found otherwise we end up with duplicate records
                    {
                        //Mutiple things could have occured:

                        //Is there a new outage? (There was no outage stored in memory, and the one we just pulled from the server has one!)
                        if (current.Outage == null && o.Outage != null)
                        {
                            //Assign the new outage to the Cached version
                            current.Outage = o.Outage;

                            //write to the log
                            Console.WriteLine("NEW OUTAGE");
                            current.Log.Add(new LogEntry($"{DateTime.Now} - {o.Suburb}: Outage Occurred", s =>
                            {
                                GlobalLog.Add(new LogEntry(s, null));
                                RaisePropertyChanged("GlobalLog");
                            }));
                            
                            return;
                        };

                        //Is an outage complete? (The outage was not null, but the new version says it is now null)
                        if (current.Outage != null && o.Outage == null)
                        {
                            //Could be just as simple to say current.Outage == null; But I do this for a better understanding of "why is it null?"
                            current.Outage = o.Outage;
                            Console.WriteLine("SERVICE RESTORED");
                            current.Log.Add(new LogEntry($"{DateTime.Now} - {current.Suburb}: Service Restored", s =>
                            {
                                GlobalLog.Add(new LogEntry(s, null));
                                RaisePropertyChanged("GlobalLog");
                            }));

                            return;
                        };

                        //Is there a modification to outage? (Outage estimation time is no longer what it used to be)
                        if (current?.Outage?.EstimatedRestorationTimeString != o?.Outage?.EstimatedRestorationTimeString)
                        {                       
                            Console.WriteLine("MODIFICATION TO OUTAGE");
                            current.Log.Add(new LogEntry($"{DateTime.Now} - {o.Suburb}: ETA modified from {current.Outage.EstimatedRestorationTimeString} to {o.Outage.EstimatedRestorationTimeString}", s =>
                            {
                                GlobalLog.Add(new LogEntry(s, null));
                                RaisePropertyChanged("GlobalLog");
                            }));

                            //Logging should be done prior to changing otherwise I lose what the old value used to be
                            current.Outage = o.Outage;

                            return;
                        };

                    }
                });

            Console.WriteLine("END OF UPDATE");

        }

        private IPowerOutageService _poweroutageService;
        private string _SearchText = "";
        private object _SuburbLock = new object();
        private bool _ShowOperational = false;
        
        private object _GlobalLogLock = new object(); //Lock object for mutex access to the global logger

        public MainPageViewModel(IPowerOutageService powerOutageService)
        {
            _poweroutageService = powerOutageService;

            //These Binding operations are important! It allows the observable collection to be modified outside of the thread they were defined in.
            BindingOperations.EnableCollectionSynchronization(Suburbs, _SuburbLock);
            BindingOperations.EnableCollectionSynchronization(GlobalLog, _GlobalLogLock);

        }

        


    }




}
