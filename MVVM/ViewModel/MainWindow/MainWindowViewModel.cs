﻿using PowerOutageTester.MVVM.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PowerOutageTester.MVVM.ViewModel
{
    public class MainWindowViewModel : IMainWindowViewModel
    {
        public Page MainPage { get; set; }
        public MainWindowViewModel(MainPage mainPage)
        {
            MainPage = mainPage;
        }
    }
}
