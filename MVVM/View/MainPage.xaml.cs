﻿using PowerOutageTester.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PowerOutageTester.MVVM.View
{
    [PippyPlatform.Attributes.PippyCategory("UNCATEGORIZED")]
    public partial class MainPage : Page
    {
        public MainPage(IMainPageViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel; //This seems to be the easiest way for me to bind the data context. I'm yet to try binding it in the XAML but not sure how my platform will take it
        }
    }
}
