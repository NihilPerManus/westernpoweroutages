﻿using PowerOutageTester.Notification;
using System;

namespace PowerOutageTester.Logging
{
    public class LogEntry : PippyPlatform.Observable.ObservableClass
    {
        static INotifyUser localToast;
        private string _Entry = "";

        string Entry { get { return _Entry; } set { _Entry = value; RaisePropertyChanged(); } }

        public LogEntry(string logMessage, Action<string> callback)
        {
            System.Console.WriteLine("trying to add string " + logMessage);
            Entry = logMessage;
            if (callback != null)
            {
                callback?.Invoke(logMessage);
                //This was done to test the toast notification. as you can see there is a tight coupling to the Toast service when I wanted it to be whatever Notification system is attached (i.e email, sms, toast, pushbullet etc.)
                localToast = new Toast(new Recipient() { DisplayName = "Pippy", Message = logMessage, Heading = "Event!", Username = "This PC" });

                //If the local toast has been defined then fire away!
                localToast?.Notify();
            }
            
            Console.WriteLine("Callback done");

        }


        public override string ToString()
        {
            return Entry;
        }
    }
}