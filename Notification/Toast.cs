﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace PowerOutageTester.Notification
{
    //This class allows for a toast notification to show to the user in Windows 10.
    public class Toast : INotifyUser
    {
        IRecipient _recipient;
        public Toast(IRecipient recipient)
        {
            _recipient = recipient;
        }

        public void Notify()
        {
            if(_recipient != null)
                SendToast();
        }

        //Sends a toast to the PC
        private void SendToast()
        {
            ToastContent content = new ToastContent()
            {
       
                Visual = GetToastVisual(),
                Actions = GetToastActionsCustom(),
                Audio = GetToastAudio(),
            };

            //Please not this is not that same XmlDocument provided by System Namespace. This seems to be a different version provided specifically for Toast Notifications
            XmlDocument document = new XmlDocument();

            //Load the XML serialised version of the content object
            document.LoadXml(content.GetContent());

            //create the new notification
            ToastNotification toast = new ToastNotification(document);

            //Send the notification NOW
            ToastNotificationManager.CreateToastNotifier("com.PoweroutageTester.Pippy").Show(toast);
        }
        private ToastAudio GetToastAudio()
        {
            return null; //no sound?
        }

        private IToastActions GetToastActionsCustom()
        {
            //No actions required for the notification
            return null;
        }

        private ToastVisual GetToastVisual()
        {
            return new ToastVisual()
            {
                BindingGeneric = GetBindingGeneric()
            };
        }

        //Get generic toast look
        private ToastBindingGeneric GetBindingGeneric()
        {
            //generic look: Heading, then the body
            return new ToastBindingGeneric()
            {
                Children =
                {
                    new AdaptiveText()
                    {
                        Text = _recipient.Heading
                    },

                     new AdaptiveText()
                    {
                        Text = _recipient.Message
                    },


                }
            };
        }
    
    }
}
