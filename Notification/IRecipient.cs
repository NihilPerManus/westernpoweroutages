﻿namespace PowerOutageTester.Notification
{
    public interface IRecipient
    {
        string DisplayName { get; set; }
        string Username { get; set; }
        string Heading { get; set; }
        string Message { get; set; }
        string Footer { get; set; }
        string Metadata { get; set; }
    }
}