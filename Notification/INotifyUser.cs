﻿namespace PowerOutageTester.Notification
{
    public interface INotifyUser
    {
        void Notify();
    }
}