﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//A state class. Just holds state, and has no need for methods and is consumed by another class. 
namespace PowerOutageTester.Notification
{
    class Recipient : IRecipient
    {
        public string DisplayName { get; set; }

        public string Footer { get; set; }

        public string Heading { get; set; }

        public string Message { get; set; }

        public string Metadata { get; set; }

        public string Username { get; set; }
    }
}
