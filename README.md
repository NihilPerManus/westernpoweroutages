# Western Power Outages #

After finding the JSON data that western power uses to provide feedback to the public, I decided it would be fun to create a monitoring tool which would show all the results in an interface of some sort.
There are multiple ways I would like to show this information but for now I've placed it in a very basic way just to show that the data is in an easy way to process. refreshes the results every minute because I can't stand being 60+ seconds out of the loop!

### What is this repository for? ###

* Western Power Outage Monitoring.
* (Note: Compatible with the PippyPlatform. Place in the platforms "/plugins/application/WesternPower" folder then run the platform)

### How do I get set up? ###

* simply compile and run the application

### Contribution guidelines ###

The method for retrieving all the data via JSON.NET is done however I am yet to implement a loading screen, or mess around with the UI/UX to make it look presentable, for now it just works as I wanted. Feel free to modify the way you see fit.

