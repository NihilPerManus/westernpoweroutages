﻿using PowerOutageTester.MVVM.View;
using PowerOutageTester.MVVM.ViewModel;
using PowerOutageTester.Outage;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester._CompositionRoot
{
    [PippyPlatform.Attributes.PippyType("APPLICATION")]
    public class CompositionRoot
    {

        [STAThread]
        static void Main()
        {
            Container container = new Container();

            //Using reference technique to modify the container. Not the best practice but it works for both locall running and running via platform
            Bootstrap(container);

            //As an entry point, it should register the mainwindow and its viewmodel. The contructor does not need this.
            container.Register<MainWindow>();
            container.Register<IMainWindowViewModel, MainWindowViewModel>();

            container.Verify();
            RunApplication(container);
        }

        private static void RunApplication(Container container)
        {

                App app = new App();
                MainWindow mainWindow = container.GetInstance<MainWindow>();
                mainWindow.DataContext = container.GetInstance<IMainWindowViewModel>();
                app.Run(mainWindow);

        }

        private static void Bootstrap(Container container)
        {
            //Register the mainpage and its viewmodel
            container.Register<MainPage>();
            container.Register<IMainPageViewModel, MainPageViewModel>();

            bool mock = false; //Change this to use the mock class for giggles
            if(mock)
                container.Register<IPowerOutageService, MockOutageService>(); //If we are mocking then anything which requires am IPowerOutageService will get the mock class instead of the proper one.
            else
                container.Register<IPowerOutageService, PowerOutageService>();

            container.Register<IPowerOutageModel, PowerOutageModel>();
        }

        public CompositionRoot(ref Container sharedContainer)
        {
            Bootstrap(sharedContainer);
        }

    }
}
