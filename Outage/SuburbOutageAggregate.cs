﻿using PowerOutageTester.Logging;
using PowerOutageTester.Outage.Item;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PowerOutageTester.Outage
{
    //This binds a suburb with an outage
    public class SuburbOutageAggregate : PippyPlatform.Observable.ObservableClass
    {
        private SuburbItem _Suburb;
        private OutageItem _Outage;
        private ObservableCollection<LogEntry> _Log = new ObservableCollection<LogEntry>();
        private object _LogLock = new object();

        public SuburbItem Suburb { get { return _Suburb; } set { _Suburb = value; RaisePropertyChanged(); } }
        public OutageItem Outage { get { return _Outage; } set { _Outage = value; RaisePropertyChanged(); } }
        public ObservableCollection<LogEntry> Log { get { return _Log; } set { _Log = value; RaisePropertyChanged(); } }

        public SuburbOutageAggregate()
        {
            BindingOperations.EnableCollectionSynchronization(_Log, _LogLock);
            Log.CollectionChanged += (s, e) => RaisePropertyChanged("Log");
        }
        
    }
}
