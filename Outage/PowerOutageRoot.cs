﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage
{
    //This class holds the entire JSON rip from Western Powers API.
    class PowerOutageRoot
    {
        public List<Suburblist> SuburbList { get; set; }
        public List<Outagelist> OutageList { get; set; }
    }

    public class Suburblist
    {
        public string Name { get; set; }
        public string Postcode { get; set; }
 }
    public class Outagelist
    {
        public string Suburb { get; set; }
        public int AffectedCustomers { get; set; }
        public DateTime EstimatedRestorationTime { get; set; }
        public bool MultipleIncidents { get; set; }
        public int AmountOfIncidents { get; set; }
        public string IncidentId { get; set; }
        public string Postcode { get; set; }
        public string EstimatedRestorationTimeString { get; set; }
    }
}
