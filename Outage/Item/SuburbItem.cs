﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage.Item
{
    //Observable class. This has the nifty "RaisePropertyChanged() method which allows a property to Update the UI when there is a change.
    public class SuburbItem : PippyPlatform.Observable.ObservableClass
    {
        private string _Name;
        private string _Postcode;

        public string Name { get { return _Name; } set { _Name = value; RaisePropertyChanged(); } }
        public string Postcode { get { return _Postcode; } set { _Postcode = value; RaisePropertyChanged(); } }

        public override string ToString()
        {
            return $"{Name} [{Postcode}]";
        }

    }
}
