﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage.Item
{
    //This class inherits from observable class since changes in this class actually affec the UI.
    //The RaisePropertyChanged(); method comes from the PippyPlatform ObservableClass which means I don't need to constantly write that stuff out...
    public class OutageItem : PippyPlatform.Observable.ObservableClass
    {
        private int _AffectedCustomers;
        private DateTime _EstimatedRestorationTime;
        private bool _MultipleIncidents;
        private int _AmountOfIncidents;
        private string _IncidentId;
        private string _Postcode;
        private string _Suburb;
        private string _EstimatedRestorationTimeString;

        public string Suburb { get { return _Suburb; } set { _Suburb = value; RaisePropertyChanged(); } }
        public int AffectedCustomers { get { return _AffectedCustomers; } set{ _AffectedCustomers = value; RaisePropertyChanged(); } }
        public DateTime EstimatedRestorationTime { get { return _EstimatedRestorationTime; } set { _EstimatedRestorationTime = value; RaisePropertyChanged(); } }
        public bool MultipleIncidents { get { return _MultipleIncidents; } set { _MultipleIncidents = value; RaisePropertyChanged(); } }
        public int AmountOfIncidents { get { return _AmountOfIncidents; } set { _AmountOfIncidents = value; RaisePropertyChanged(); } }
        public string IncidentId { get { return _IncidentId; } set { _IncidentId = value; RaisePropertyChanged(); } }
        public string Postcode { get { return _Postcode; } set { _Postcode = value; RaisePropertyChanged(); } }
        public string EstimatedRestorationTimeString { get { return _EstimatedRestorationTimeString; } set { _EstimatedRestorationTimeString = value; RaisePropertyChanged(); } }
    }
}
