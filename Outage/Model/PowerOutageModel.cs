﻿using PowerOutageTester.Outage.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage
{
    //This is where the fun really begins
    class PowerOutageModel : IPowerOutageModel
    {
        //Private client
        RestSharp.RestClient client;
        //form the request as RestSharps RestRequest
        RestSharp.IRestRequest request = new RestSharp.RestRequest(){ Method = RestSharp.Method.GET };

        //rootInfo cache
        PowerOutageRoot rootInfo;

        public PowerOutageModel()
        {
            //Initialise the client pulling the JSON data down. 
            client = new RestSharp.RestClient("https://westernpower.com.au/Umbraco/Api/PowerInformation/GetPowerOutageInformation")
            {
                Proxy = WebRequest.GetSystemWebProxy()             
            };
            client.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials; //Use the default Network credentials just incase you live behind a proxy

            GetRoot();
        }

        //Get the root data from the server. This is ALL the information Western Power provides
        private void GetRoot()
        {
            rootInfo = client.Execute<PowerOutageRoot>(request).Data;
        }

        //This is the exposed version of GetRoot. Abstraction just incase there was more to it than just creating the request. Turns out there wasn't haha.
        public void Update()
        {
            GetRoot();
        }

        //Convert xList to xItem. This is because of the way RestSharps default JSON parser creates object based on the Name in the JSON data
        //In this case, the parent item in the Data pulled from the server is "SuburbItem" a class must be called SuburbItem. 
        //I believe there are other implementations that use attributes to help with the conversion but for now I created an adapter function
        public IEnumerable<SuburbItem> GetSuburbs()
        {

            return rootInfo?.SuburbList
                ?.Select(s => new SuburbItem()
                {
                    Postcode = s.Postcode,
                    Name = s.Name
                });
        }

        //As with outages...
        public IEnumerable<OutageItem> GetOutages()
        {
            return rootInfo?.OutageList
                ?.Select(o => new OutageItem()
                {
                    AffectedCustomers = o.AffectedCustomers,
                    AmountOfIncidents = o.AmountOfIncidents,
                    EstimatedRestorationTime = o.EstimatedRestorationTime,
                    EstimatedRestorationTimeString = o.EstimatedRestorationTimeString,
                    IncidentId = o.IncidentId,
                    MultipleIncidents = o.MultipleIncidents,
                    Postcode = o.Postcode,
                    Suburb = o.Suburb
                });
        }
    }
}
