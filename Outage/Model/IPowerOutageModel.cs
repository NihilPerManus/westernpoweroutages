﻿using System.Collections.Generic;
using PowerOutageTester.Outage.Item;

namespace PowerOutageTester.Outage
{
    public interface IPowerOutageModel
    {
        IEnumerable<OutageItem> GetOutages();
        IEnumerable<SuburbItem> GetSuburbs();
        void Update();
    }
}