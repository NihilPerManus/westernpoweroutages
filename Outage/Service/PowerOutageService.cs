﻿using PowerOutageTester.Outage.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage
{
    public class PowerOutageService : IPowerOutageService
    {
        private IPowerOutageModel _outageModel;

        public PowerOutageService(IPowerOutageModel outageModel)
        {
            _outageModel = outageModel;
        }


        //Query the current state of outages
        public IEnumerable<SuburbOutageAggregate> GetOutages()
        {
            var outages = _outageModel.GetOutages(); //Get all the outages
            var suburbs = _outageModel.GetSuburbs(); //Get all the Suburbs

            //This line is important to update the data inside the model
            _outageModel.Update();

            return suburbs //Get all the suburbs
                ?.Select(s => new SuburbOutageAggregate()
                {
                    Suburb = s, // assign the current suburb to this item
                    Outage = outages.FirstOrDefault(o => o.Suburb.ToLower() == s.Name.ToLower()) //Assign the collection of outages to this based on the name... think of the name as a foreign key.
                }) // return them in the form of a SuburbOutageAggregate
                ?.OrderByDescending(s => s.Outage?.EstimatedRestorationTimeString); //Order them by the time it will probably take to resolve
                

        }
        

    }
}
