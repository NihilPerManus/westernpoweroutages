﻿using System.Collections.Generic;
using PowerOutageTester.Outage.Item;

namespace PowerOutageTester.Outage
{
    public interface IPowerOutageService
    {
        IEnumerable<SuburbOutageAggregate> GetOutages();
    }
}