﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerOutageTester.Outage
{
    //This is the kind of item you could use in unit testing so you sdon't need to test with live data. This is fake, known-state data.
    class MockOutageService : IPowerOutageService
    {

        //store how many outages occur
        int num = 0;

        //This is different. every time outages is called, the count rises and simulates a change in restoration. Just so I can test a change between pulls.
        public IEnumerable<SuburbOutageAggregate> GetOutages()
        {
            num++;
            if(num <= 3 && num > 1)
                return new List<SuburbOutageAggregate>()
                {
                

                   new SuburbOutageAggregate()
                        {
                            Outage = new Item.OutageItem() { EstimatedRestorationTimeString = $"{num} days", MultipleIncidents = true, AmountOfIncidents = 666 },
                            Suburb = new Item.SuburbItem() { Name = "Mocksville", Postcode = "9999" },

                        }
                };
            //after the 3 pulls, this just returns a normal suburb which then raises that an outage was restored allowing me to test the completion of an outage
            return new List<SuburbOutageAggregate>()
            {


                new SuburbOutageAggregate()
                    {
                        //Outage = new Item.OutageItem() { EstimatedRestorationTimeString = $"{num} days" }, fixed yay!
                        Suburb = new Item.SuburbItem() { Name = "Mocksville", Postcode = "9999" },

                    }
            };
        }
    }
}
