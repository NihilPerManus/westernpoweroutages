﻿using PowerOutageTester.Logging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace PowerOutageTester.Converter
{
    //Converters are usually self descri[tive however still need detailed explanations.
    //This pretty much is used to Show or hide a particular control should the log collection be empty
    public class PropertyNullValuetoVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //only work when the value we are testing is  the following type:
            if(value is ObservableCollection<LogEntry>)
            {
                //If there are no entries
                if ((value as ObservableCollection<LogEntry>).Count() > 0)
                {
                    //hide it!
                    return Visibility.Visible;
                }
            }

            //otherwise show it!
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
