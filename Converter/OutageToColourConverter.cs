﻿using PowerOutageTester.Outage.Item;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace PowerOutageTester.Converter
{
    //returns green if there are no outages associated with a pased value, otherwise shows red
    class OutageToColourConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is OutageItem)
            {
                //Show a red colour
                return "#FFAA0000";
            }
            else
            {
                //Show a green colour
                return "#FF00AA00";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
